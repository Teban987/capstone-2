const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")
const orderRoutes = require("./routes/orderRoutes")

const app = express();
const port = 3000;

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({
	extended: true
}));

app.use("/product", productRoutes)
app.use("/users", userRoutes)
app.use("/order", orderRoutes)

mongoose.connect("mongodb+srv://admin:admin123@cluster0.0ca0s.mongodb.net/bike_ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => {
	console.log("Now connected to MongoDB Atlas.")
})

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${
		process.env.PORT || port
	}`)
})
