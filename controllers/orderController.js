const Order = require("../models/order")


//creating order
module.exports.checkout = (body) => {

	const newOrder = new Order ({

		userId:	body.userId,
		itemToOrder: body.itemToOrder,
		totalAmount: body.totalAmount
	})

	return newOrder.save().then((order, error) => {

		if(error){
			return false;
		}else{
			return true;
		}
	})
}

//get specific user's order
module.exports.getOrder = (body) => {

	return Order.find({userId: body}).then(result => {
		if(result.length === 0){
			return "No order placed. Please make an order."
		}else{
			return result
		}
	})
}

//get all orders (admin only)
module.exports.getOrders = () => {
	return Order.find().then(result => {

		return result;
	})
}
