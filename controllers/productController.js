const Product = require("../models/product")

//add single product
module.exports.addProduct = (body) => {

	let newProduct = new Product({
		name: body.name,
		description: body.description,
		price: body.price,
		material: body.material,
		size: body.size,
		brandName: body.brandName
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})

}

// get all available products
module.exports.getAllAvailable = () => {
	return Product.find({isAvailable: true}).then(result => {
		return result;
	})
}

//get specific product
module.exports.getProduct = (params) => {
	return Product.findById(params.productId).then(result => {
		return result;
	})
}

//update specific product(admin)
module.exports.updateProduct = (params, body) => {

	let updatedProduct = {
		name: body.name,
		description: body.description,
		price: body.price,
		material: body.material,
		size: body.size,
		brandName: body.brandName
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}

//archive specific(admin)
module.exports.archiveProduct = (params) => {
	let updatedProduct = {
		isAvailable: false
	}

	return Product.findByIdAndUpdate(params.productId, updatedProduct).then((product, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}



