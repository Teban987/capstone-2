const User = require("../models/user");
const Product = require("../models/product");
const Order = require("../models/order")
const auth = require("../auth");
const bcrypt = require("bcrypt");

//register user
module.exports.registerUser = (body) => {
	let newUser = new User({
		email: body.email,
		password: bcrypt.hashSync(body.password, 10),
		firstName: body.firstName,
		lastName: body.lastName
	})

	return User.find({email: body.email}).then(result => {
		if(result.length > 0){
			return "Email is already taken!"
		}else{
			return newUser.save().then((user, error) => {
				if(error) {
					return false;
				}else{
					return true;
				}
			})
		}
	})	
}

//user login
module.exports.loginUser = (body) => {
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return "Email not registered";
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())}
			}else{
				return "Incorrect password";
			}

		}
	})
}
//get user details
module.exports.getProfile = (userId) => {
	return User.findById(userId).then(result => {
		result.password = undefined; //reassign the value of the user's password to undefined for security purposes
		return result;
	})
}

//add product(admin)
module.exports.addProduct = (body) => {

	let newProduct = new Product({
		name: body.name,
		description: body.description,
		price: body.price
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})

}
//make user an admin
module.exports.makeAdmin = (body) => {

	const setAdmin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(body.userId, setAdmin).then((user, error) => {

		if(error){
			return false
		}else{
			return true
		}
	})
}
//make admin
module.exports.updateProfile = (body) => {
	let admin = {
		isAdmin: true
	}

	return User.findByIdAndUpdate(body.userId, admin).then((user, error) => {
		if(error){
			return false
		}else{
			return true
		}
	})
}



