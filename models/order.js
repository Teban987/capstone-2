const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: [true, "Total amount is required"]
	},
	puchasedOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	itemToOrder: [
		{
			productId: {
				type: String,
				required: true
			},
			quantity: {
				type: Number,
				required: true
			}
		}
	]

})

module.exports = mongoose.model("Order", orderSchema);