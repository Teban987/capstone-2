const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController')
const auth = require("../auth")


//creating order
router.post("/checkout", auth.verify, (req, res) => {

	const orderedItem = {

		userId:	auth.decode(req.headers.authorization).id,
		itemToOrder: req.body.itemToOrder,
		totalAmount: req.body.totalAmount
	}

	orderController.checkout(orderedItem).then(resultFromCheckout => res.send(resultFromCheckout))
})

//get specific order of a user
router.get("/details", auth.verify, (req, res) => {
	const orderData = auth.decode(req.headers.authorization)

	orderController.getOrder(orderData.id).then(resultFromGetOrder => res.send(resultFromGetOrder))
})

//get all orders(admin-only)
router.get("/all", auth.verify, (req, res) => {

	const allOrders = auth.decode(req.headers.authorization);

	if(allOrders.isAdmin){
		orderController.getOrders().then(resultFromgetOrders => res.send(resultFromgetOrders))
	}else{
		res.send({auth: "Request failed! User must be an admin"})
	}

})



module.exports = router;