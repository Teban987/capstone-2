const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController")

//add single product
router.post("/add", (req, res) => {
	productController.addProduct(req.body).then(resultFromAdd => res.send(resultFromAdd))
})

//get all available product
router.get("/all", (req, res) => {
	productController.getAllAvailable().then(resultFromGet => res.send(resultFromGet))
})

//get specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromGetSpecific => res.send(resultFromGetSpecific))
})

//update product info(admin)
router.put("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	
	if(token.isAdmin === true){
			productController.updateProduct(req.params, req.body).then(resultFromUpdateProduct => res.send(resultFromUpdateProduct))
		}else{
			res.send({auth: "Request failed! User must be an admin"})
		}

})

//archive specific(admin)
router.delete("/:productId", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)
	
	if(token.isAdmin === true){
			productController.archiveProduct(req.params, req.body).then(resultFromDeleteProduct => res.send(resultFromDeleteProduct))
		}else{
			res.send({auth: "Request failed! User must be an admin"})
		}

})




module.exports = router;