const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");

//register user
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

//user login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//get user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile(userData.id).then(resultFromProfile => res.send(resultFromProfile))
})

//add new product(admin)
router.post("/", auth.verify, (req, res) => {
	let token = auth.decode(req.headers.authorization)

	if(token.isAdmin === true){
		userController.addProduct(req.body).then(resultFromAddProduct => res.send(resultFromAddProduct))
	}else{
		res.send({auth: "Request failed! User must be an admin"})
	}
})

//set user as admin(stretch)
router.put("/makeAdmin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		userController.makeAdmin(req.body).then(resultFromSetAdmin => res.send(resultFromSetAdmin))
	}else{
		res.send({auth: "Request failed! User must be an admin"})
	}
})

//make admin
router.put("/userAdmin", (req, res) => {
	userController.updateProfile(req.body).then(resultFromUpdate => res.send(resultFromUpdate))
})




module.exports = router;